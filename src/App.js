import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
// import { renderRoutes } from 'react-router-config';
import Loadable from 'react-loadable';
import './App.scss';
import firebase from 'firebase'

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = Loadable({
  loader: () => import('./containers/DefaultLayout'),
  loading
});

// Pages
const Login = Loadable({
  loader: () => import('./menu/auth/login'),
  loading
});

class App extends Component {

  render() {
    return (
      <HashRouter>
          <Switch>
            <Route exact path="/login" name="Login Page" component={Login} />
            <Route path="/" name="Home" component={DefaultLayout} />
          </Switch>
      </HashRouter>
    );
  }
}
            // <Route exact path="/register" name="Register Page" component={Register} />

export default App;

var config = {
    apiKey: "AIzaSyBD5TvUvDFEV4fnvI-EyfeC7HJZ_9TFJ7A",
    authDomain: "limafoundation.firebaseapp.com",
    databaseURL: "https://limafoundation.firebaseio.com",
    projectId: "limafoundation",
    storageBucket: "limafoundation.appspot.com",
    messagingSenderId: "964837701157",
    appId: "1:964837701157:web:0006cfdf1c7cc094"
  };
  firebase.initializeApp(config);
