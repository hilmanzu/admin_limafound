import React, { Component } from 'react';
import { Link,Route } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import firebase from 'firebase'
import swal from 'sweetalert2';

class Login extends Component {

  constructor(props){
  super(props);
  this.handleChange = this.handleChange.bind(this);
  this.handleChange2 = this.handleChange2.bind(this);
  this.state = {
    email:'',
    password:'',
    loading: true,
    dashboard:''
    }
  }

  componentDidMount(){
    var data = localStorage.getItem('myData');
    if (data === '6buZcPyKzXRkCccLOpx1kYQfmQ23'){
      this.props.history.push('/')
    }else{
      this.props.history.push('/login')
    }
  }

  handleChange(event) {
    this.setState({email: event.target.value});
  }

  handleChange2(event) {
    this.setState({password: event.target.value});
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Email" autoComplete="username" value={this.state.email} onChange={this.handleChange} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password" value={this.state.password} onChange={this.handleChange2} />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" onClick={this.login}>Login</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Selamat Datang</h2>
                      <p>Aplikasi ini khusus untuk pengguna Admin Lima Foundation</p>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }

  login = () => {
    if (this.state.email == ''){
      alert('Isi Email anda')
    }else if (this.state.password == ''){
      alert('Isi Password anda')
    }else{
      const { email, password } = this.state;
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(async(responseJson) => {
           localStorage.setItem('myData',responseJson.user.uid);
           window.location.reload();
        })
        .catch((error) => {
          if ( error.code == 'auth/user-not-found' ){
          swal.fire('Akun Tidak ditemukan')
          }else if ( error.code == 'auth/wrong-password'){
          swal.fire('password anda salah')
          }
        });
    }
  }
}

export default Login;
