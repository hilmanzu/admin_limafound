import React, { Component } from 'react';
import {
  Badge,
  Button,
  ButtonDropdown,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Row,
  Table,Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';

import firebase from 'firebase/app';
import 'firebase/storage';
import swal from 'sweetalert2';

class Header extends Component {
  constructor(props) {
    super(props);
    this.judul = this.judul.bind(this);
    this.subjudul = this.subjudul.bind(this);
    this.state = {

      judul : '',
      subjudul  : '',
      data  : {},
      url   : '',
      image : null,
      loading: true,

    }
  }

  componentDidMount(){
       var db = firebase.firestore();
       var docRef = db.collection("web_limaf").doc("0.data").collection("Header").doc("data")
       docRef.onSnapshot( async (doc) => {
        let data = doc.data()
            this.setState({
                ...data
            })
        })
  }

  render() {
    const {data} = this.state

    return (
      <div >
        <Row>
          <Col xs="12" md="7">
            <Card>
              <CardHeader>
                <strong>Edit Data</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Judul</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.judul} onChange={this.judul} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Subjudul</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <textarea value={this.state.subjudul} onChange={this.subjudul} type="text" id="text-input" name="text-input" style={{padding:10,width:300,height:300}}/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Gambar</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <img style = {{width:150,height:150}} src={this.state.img}/>
                      <Input style = {{marginTop:10}} type="file" id="file-input" name="file-input" onChange={this.handleChange2}/>
                    </Col>
                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter row>
                <div hidden={this.state.button}>
                  <Button style={{marginRight:10}} size="sm" color="primary" onClick={this.handleUpload}><i className="fa fa-dot-circle-o"></i> Update</Button>
                </div>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }

  handleUpload = () => {
      const {image} = this.state;
      if (image == null){
        const db = firebase.firestore()
        const upload = db.collection("web_limaf").doc("0.data").collection("Header").doc("data")
        upload.update({
          judul      : this.state.judul,
          subjudul  : this.state.subjudul,
        })
        .then(()=>{
          return swal.fire("data telah diganti")
        })
      }

      else{
      const storage = firebase.storage()
      const uploadTask = storage.ref(`images/${image.name}`).put(image);
      uploadTask.on('state_changed', 
      (snapshot) => {
        // progrss function ....
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.setState({progress});
      }, 
      (error) => {
           // error function ....
        console.log(error);
      }, 
      () => {
          // complete function ....
          storage.ref('images').child(image.name).getDownloadURL().then((url) => {
              this.setState({url:url})
              swal.fire({
                    text: "berhasil",
                    closeOnClickOutside: false})
              .then(willDelete => {
                if (willDelete) {
                const db = firebase.firestore()
                const upload = db.collection("web_limaf").doc("0.data").collection("Header").doc("data")
                upload.update({
                    img       : this.state.url,
                    judul     : this.state.judul,
                    subjudul  : this.state.subjudul
                })
                .then(()=>{
                  return swal.fire("Data telah diganti")
                  })
                }
              })
          })
      })
    }
  }






  judul(event) {
    this.setState({judul: event.target.value});
  }
  subjudul(event) {
    this.setState({subjudul: event.target.value});
  }
  owner(event) {
    this.setState({owner: event.target.value});
  }
  handleChange2 = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      this.setState(() => ({image}));
    }
  }

}

export default Header;
