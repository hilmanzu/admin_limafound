import React, { Component } from 'react';
import {
  Badge,
  Button,
  ButtonDropdown,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Row,
  Table,Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';

import firebase from 'firebase/app';
import 'firebase/storage';
import swal from 'sweetalert2';

class Header extends Component {
  constructor(props) {
    super(props);
    this.alamat = this.alamat.bind(this);
    this.ponsel = this.ponsel.bind(this);
    this.email = this.email.bind(this);
    this.judul = this.judul.bind(this);
    this.subjudul = this.subjudul.bind(this);
    this.state = {

      alamat : '',
      ponsel  : '',
      email:'',
      judul:'',
      subjudul:'',
      data  : {},
      url   : '',
      image : null,
      loading: true,

    }
  }

  componentDidMount(){
       var db = firebase.firestore();
       var docRef = db.collection("web_limaf").doc("0.data").collection("Contact").doc("data")
       docRef.onSnapshot( async (doc) => {
        let data = doc.data()
            this.setState({
                ...data
            })
        })
  }

  render() {
    const {data} = this.state

    return (
      <div >
        <Row>
          <Col xs="12" md="7">
            <Card>
              <CardHeader>
                <strong>Edit Data</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">judul</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.judul} onChange={this.judul} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">subjudul</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.subjudul} onChange={this.subjudul} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">alamat</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.alamat} onChange={this.alamat} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">ponsel</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.ponsel} onChange={this.ponsel} type="text" id="text-input" name="text-input"/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Email</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.email} onChange={this.email} type="text" id="text-input" name="text-input"/>
                    </Col>
                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter row>
                <div hidden={this.state.button}>
                  <Button style={{marginRight:10}} size="sm" color="primary" onClick={this.handleUpload}><i className="fa fa-dot-circle-o"></i> Update</Button>
                </div>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }

  handleUpload = () => {
    const db = firebase.firestore()
    const upload = db.collection("web_limaf").doc("0.data").collection("Contact").doc("data")
    upload.update({
      alamat      : this.state.alamat,
      ponsel  : this.state.ponsel,
      email  : this.state.email,
      judul  : this.state.judul,
      subjudul  : this.state.subjudul,
    })
    .then(()=>{
      return swal.fire("data telah diganti")
    })
  }

  alamat(event) {
    this.setState({alamat: event.target.value});
  }
  ponsel(event) {
    this.setState({ponsel: event.target.value});
  }
  email(event) {
    this.setState({email: event.target.value});
  }
  judul(event) {
    this.setState({judul: event.target.value});
  }
  subjudul(event) {
    this.setState({subjudul: event.target.value});
  }
  handleChange2 = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      this.setState(() => ({image}));
    }
  }

}

export default Header;
