import React, { Component } from 'react';
import {
  Badge,
  Button,
  ButtonDropdown,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Row,
  Table,Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';

import firebase from 'firebase/app';
import 'firebase/storage';
import swal from 'sweetalert2';

class Aboutlist extends Component {
  constructor(props) {
    super(props);
    this.nama = this.nama.bind(this);
    this.asal = this.asal.bind(this);
    this.urutan = this.urutan.bind(this);
    this.state = {

      nama : '',
      asal  : '',
      data  : {},
      urutan : '',
      image: null,
      url   : '',
      loading: true,

    }
  }

  componentDidMount(){
       var id = localStorage.getItem('id');
       var item = localStorage.getItem('item');
       var db = firebase.firestore();
       var docRef = db.collection("web_limaf").doc("0.data").collection(item).doc(id)
       docRef.onSnapshot( async (doc) => {
        let data = doc.data()
            this.setState({
                ...data
            })
        })
  }

  render() {
    const {data} = this.state

    return (
      <div >
        <Row>
          <Col xs="12" md="7">
            <Card>
              <CardHeader>
                <strong>Edit Data</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Nama</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.nama} onChange={this.nama} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Asal</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.asal} onChange={this.asal} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Gambar</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <img style = {{width:150,height:150}} src={this.state.img}/>
                      <Input style = {{marginTop:10}} type="file" id="file-input" name="file-input" onChange={this.handleChange2}/>
                    </Col>
                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter row>
                <div hidden={this.state.button}>
                  <Button style={{marginRight:10}} size="sm" color="primary" onClick={this.handleUpload}><i className="fa fa-dot-circle-o"></i> Update</Button>
                </div>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }

  handleUpload = () => {
      var id = localStorage.getItem('id');
      var item = localStorage.getItem('item');
      const {image} = this.state;
      if (image == null){
        const db = firebase.firestore()
        const upload = db.collection("web_limaf").doc("0.data").collection(item)
        upload.add({
          nama      : this.state.nama,
          asal  : this.state.asal
        })
        .then(()=>{
          window.location.reload()
          return swal.fire("data telah diganti")
        })
      }

      else{
      const storage = firebase.storage()
      const uploadTask = storage.ref(`image/${image.name}`).put(image);
      uploadTask.on('state_changed', 
      (snapshot) => {
        // progrss function ....
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.setState({progress});
      }, 
      (error) => {
           // error function ....
        console.log(error);
      }, 
      () => {
          // complete function ....
          storage.ref('image').child(image.name).getDownloadURL().then((url) => {
              this.setState({url:url})
              swal.fire({
                    text: "berhasil",
                    closeOnClickOutside: false})
              .then(willDelete => {
                if (willDelete) {
                var id = localStorage.getItem('id');
                var item = localStorage.getItem('item');
                const db = firebase.firestore()
                const upload = db.collection("web_limaf").doc("0.data").collection(item)
                upload.add({
                    img     : this.state.url,
                    nama     : this.state.nama,
                    asal  : this.state.asal
                })
                .then(()=>{
                  window.location.reload()
                  return swal.fire("Data telah diganti")
                  })
                }
              })
          })
      })
    }
  }






  nama(event) {
    this.setState({nama: event.target.value});
  }
  asal(event) {
    this.setState({asal: event.target.value});
  }
  urutan(event) {
    this.setState({urutan: event.target.value});
  }
  handleChange2 = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      this.setState(() => ({image}));
    }
  }

}

export default Aboutlist;
