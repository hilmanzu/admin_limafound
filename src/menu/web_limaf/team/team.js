import React, { Component } from 'react';
import {
  Badge,
  Button,
  ButtonDropdown,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Row,
  Table,Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';

import firebase from 'firebase/app';
import 'firebase/storage';
import swal from 'sweetalert2';
var ReactTable = require("react-table").default;

class editbody2 extends Component {
  constructor(props) {
    super(props);
    this.judul = this.judul.bind(this);
    this.subjudul = this.subjudul.bind(this);
    this.owner = this.owner.bind(this);
    this.state = {

      judul : '',
      subjudul  : '',
      data  : {},
      owner : '',
      image   : null,
      url   : '',
      loading: true,
      items:[]

    }
  }

  componentDidMount(){
       var db = firebase.firestore();
       var docRef = db.collection("web_limaf").doc("0.data").collection("Team_header").doc("data")
       const item = db.collection("web_limaf").doc("0.data").collection("Team_list").orderBy('urutan', 'asc')
       
       docRef.onSnapshot( async (doc) => {
        let data = doc.data()
            this.setState({
                ...data
            })
        })

       item.onSnapshot( async (doc) => {
        var data = []
          doc.forEach((docs)=>{
            let item = data
            item.push({
              data : docs.data(),
              id   : docs.id
            })
          })
        this.setState({
          items : data,loading:false
        })
      })
  }

  render() {
    const {data,items} = this.state
    const columns = [{
                      Header: 'No',
                      id: 'no',
                      width: 40,
                      style: {textAlign: 'center'},
                      Cell: (row) => {return <div>{row.index+1}</div>}
                    },{
                      Header: 'Nama Fitur',
                      id: 'nama',
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <td ><p>{d.data.nama}</p></td>
                        </div>
                    },{
                      Header: 'Logo',
                      id: 'status',
                      width: 60,
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                          <img style={{width:40,height:40}} src={d.data.img}/>
                        </div>
                    },{
                      id: 'Edit',
                      Header: 'Edit',
                      width: 100,
                      accessor: d => <div style={{textAlign:'center'}} onClick={()=>{localStorage.setItem('item','Team_list');localStorage.setItem('id',d.id);this.props.history.push('/List_team')}}><a className="btn btn-success" title="Edit" ><i className="fa fa-info"></i> </a></div>
                    },{
                      id: 'delete',
                      Header: 'Hapus Data',
                      width: 100,
                      accessor: d => 
                        <div style={{textAlign:'center'}}>
                            <a onClick=
                              {()=>{ 
                                      swal.fire("ingin menghapus data?")
                                      .then(willDelete => {
                                        if (willDelete) {
                                          const db = firebase.firestore()
                                          db.collection("web_limaf").doc("0.data").collection("Team_list").doc(d.id).delete()
                                          return swal.fire("data telah dihapus");
                                        }
                                      })
                                      .then(() => {
                                        window.location.href = '#/Team'
                                      })
                                      .catch(function(error) {
                                          swal.fire("Error removing document: ", error);
                                      });

                                   }} className="btn btn-danger" title="Edit">
                              <i className="fa fa-trash-o"></i> 
                            </a>
                        </div>
                    }]

    return (
      <div >
        <Row>
          <Col xs="12" md="7">
            <Card>
              <CardHeader>
                <strong>Edit Data</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Judul</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.judul} onChange={this.judul} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Subjudul</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <textarea value={this.state.subjudul} onChange={this.subjudul} type="text" id="text-input" name="text-input" style={{padding:10,width:300,height:300}}/>
                    </Col>
                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter row>
                <div hidden={this.state.button}>
                  <Button style={{marginRight:10}} size="sm" color="primary" onClick={this.handleUpload}><i className="fa fa-dot-circle-o"></i> Update</Button>
                </div>
              </CardFooter>
            </Card>
          </Col>
        </Row>
        <div>
          <CardFooter row>
            <div>
              <Button style={{marginRight:10}} size="sm" color="primary" onClick={this.tambah}><i className="fa fa-dot-circle-o"></i> Tambah</Button>
            </div>
          </CardFooter>
          <ReactTable
              data={items}
              columns={columns}
              defaultPageSize={5}
              className="-striped -highlight"
              noDataText="Data Tidak Ditemukan"
          />
        </div>
      </div>
    );
  }

  tambah = (d) => {
    localStorage.setItem('item','Team_list');
    localStorage.setItem('id',d.id);
    this.props.history.push('/Add_team')
  }

  handleUpload = () => {
      const {image} = this.state;
      if (image == null){
        const db = firebase.firestore()
        const upload = db.collection("web_limaf").doc("0.data").collection("Team_header").doc("data")
        upload.update({
          judul      : this.state.judul,
          subjudul  : this.state.subjudul,
        })
        .then(()=>{
          return swal.fire("data telah diganti")
        })
      }

      else{
      const storage = firebase.storage()
      const uploadTask = storage.ref(`images/${image.name}`).put(image);
      uploadTask.on('state_changed', 
      (snapshot) => {
        // progrss function ....
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.setState({progress});
      }, 
      (error) => {
           // error function ....
        console.log(error);
      }, 
      () => {
          // complete function ....
          storage.ref('images').child(image.name).getDownloadURL().then((url) => {
              this.setState({url:url})
              swal.fire({
                    text: "berhasil",
                    closeOnClickOutside: false})
              .then(willDelete => {
                if (willDelete) {
                const db = firebase.firestore()
                const upload = db.collection("web_limaf").doc("0.data").collection("Team_header").doc("data")
                upload.update({
                    image     : this.state.url,
                    judul     : this.state.judul,
                    subjudul  : this.state.subjudul
                })
                .then(()=>{
                  return swal.fire("Data telah diganti")
                  })
                }
              })
          })
      })
    }
  }

  judul(event) {
    this.setState({judul: event.target.value});
  }
  subjudul(event) {
    this.setState({subjudul: event.target.value});
  }
  owner(event) {
    this.setState({owner: event.target.value});
  }
  handleChange2 = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      this.setState(() => ({image}));
    }
  }

}

export default editbody2;
