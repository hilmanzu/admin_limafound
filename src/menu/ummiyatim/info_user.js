import React, { Component } from 'react';
import {
  Badge,
  Button,
  ButtonDropdown,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Row,
  Table,Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';

import firebase from 'firebase/app';
import 'firebase/storage';
import swal from 'sweetalert2';
import provinsi from './component/provinsi.json'
import kabupaten from './component/kabupaten.json'
import kecamatan from './component/kecamatan.json'

class Header extends Component {
  constructor(props) {
    super(props);
    this.tempat_lahir = this.tempat_lahir.bind(this);
    this.tanggal_lahir = this.tanggal_lahir.bind(this);
    this.agama = this.agama.bind(this);
    this.nama = this.nama.bind(this);
    this.nik = this.nik.bind(this);
    this.status = this.status.bind(this);
    this.tanggungan = this.tanggungan.bind(this);
    this.minat = this.minat.bind(this);
    this.kondisi_eko = this.kondisi_eko.bind(this);
    this.state = {
      //data
      nik           :'',
      nama          :'',
      tempat_lahir  : '',
      tanggal_lahir : '',
      agama         :'',
      status        : '',
      tanggungan    : '',
      minat         : '',
      kondisi_eko   : '',
      ///
      item  : '',
      url   : '',
      image : null,
      loading: true,

    }
  }

  componentDidMount(){
       var id = localStorage.getItem('id_user');
       var db = firebase.firestore();
       var docRef = db.collection("user").doc(id)
       docRef.onSnapshot( async (doc) => {
        let data = doc.data()
            this.setState({
                ...data,item:data,loading:false
            })
        })
  }

  render() {
    const {item,loading} = this.state
    const provinsis = provinsi.filter((data)=> data.id === item.provinsi)
    const kabupatens = kabupaten.filter((data)=> data.id === item.kabupaten)

    if (loading === true){
      return (

        <div>Sedang Mengambil data</div>

      )
    }

    return (
      <div >
        <Row>
          <Col xs="12" md="7">
            <Card>
              <CardHeader>
                <strong>Edit Data</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Nik</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.nik} onChange={this.nik} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Nama</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.nama} onChange={this.nama} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Tempat lahir</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.tempat_lahir} onChange={this.tempat_lahir} type="text" id="text-input" name="text-input" />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Tanggal Lahir</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.tanggal_lahir} onChange={this.tanggal_lahir} type="text" id="text-input" name="text-input"/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Agama</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.agama} onChange={this.agama} type="text" id="text-input" name="text-input"/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Status</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.status} onChange={this.agama} type="text" id="text-input" name="text-input"/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Tanggungan</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.tanggungan} onChange={this.agama} type="text" id="text-input" name="text-input"/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Pekerjaan</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.pekerjaan} onChange={this.agama} type="text" id="text-input" name="text-input"/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Minat</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.minat} onChange={this.agama} type="text" id="text-input" name="text-input"/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Kondisi Ekonomi</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input value={this.state.kondisi_eko} onChange={this.agama} type="text" id="text-input" name="text-input"/>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Provinsi</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <div type="text" id="text-input" name="text-input"> {provinsis[0].name} </div>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Kabupaten</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <div type="text" id="text-input" name="text-input"> {kabupatens[0].name} </div>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Kecamatan</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <div type="text" id="text-input" name="text-input"> {this.state.kecamatan} </div>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">No Hp</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <div type="text" id="text-input" name="text-input"> {this.state.phone} </div>
                    </Col>
                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter row>
                <div hidden={this.state.button}>
                  <Button style={{marginRight:10}} size="sm" color="primary" onClick={this.handleUpload}><i className="fa fa-dot-circle-o"></i> Update</Button>
                </div>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }

  handleUpload = () => {
    var id      = localStorage.getItem('id_user');
    var db = firebase.firestore();
    var upload = db.collection("user").doc(id)
    upload.update({
      nik           : this.state.nik,
      nama          : this.state.nama,
      tempat_lahir  : this.state.tempat_lahir,
      tanggal_lahir : this.state.tanggal_lahir,
      agama         : this.state.agama,
      status        : this.state.status,
      tanggungan    : this.state.tanggungan,
      minat         : this.state.minat,
      kondisi_eko   : this.state.kondisi_eko
    })
    .then(()=>{
      return swal.fire("Sukses","","success")
    })
  }

  tempat_lahir(event) {
    this.setState({tempat_lahir: event.target.value});
  }
  tanggal_lahir(event) {
    this.setState({tanggal_lahir: event.target.value});
  }
  agama(event) {
    this.setState({agama: event.target.value});
  }
  nama(event) {
    this.setState({nama: event.target.value});
  }
  nik(event) {
    this.setState({nik: event.target.value});
  }
  status(event) {
    this.setState({status: event.target.value});
  }
  tanggungan(event) {
    this.setState({tanggungan: event.target.value});
  }
  kondisi_eko(event) {
    this.setState({kondisi_eko: event.target.value});
  }
  minat(event) {
    this.setState({minat: event.target.value});
  }
  handleChange2 = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      this.setState(() => ({image}));
    }
  }

}

export default Header;
