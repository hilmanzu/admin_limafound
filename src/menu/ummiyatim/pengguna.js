import React, { Component } from 'react';
import {
  Button,
} from 'reactstrap';
import firebase from 'firebase'
import MUIDataTable from "mui-datatables";
import swal from 'sweetalert2';
import renderIf from '../renderIf'

class bonus extends Component {

    constructor(props){
        super(props);
        this.state = {
          member :[],
          id     :'',
          nomor  :'',
          modalIsOpen: false,
          performance : '',
          synergi:'',
          integrity:'',
          bussiness:'',
        }
      }

     componentWillMount(){
       const db = firebase.firestore();
       const docRef = db.collection("user").orderBy('verifikasi', 'asc')
       docRef.onSnapshot(async(querySnapshot)=>{
        var data = []
        querySnapshot.forEach((doc)=>{
          let datas = data
          datas.push({
            id   : doc.id,
            data : doc.data()
          })
        })
        this.setState({
          member    : data,
          nomor     : Object.keys(data).length
        })
       
       })
    }

  render() {
    const {member} = this.state;

    const data = Object.values(member).map((data,index)=>
        [ 
          '00' + data.data.id_registrasi,
          data.data.nik,
          data.data.nama,
          data.data.pendaftaran,
          <div>
            {renderIf(data.data.verifikasi === true)(
            <div>
              <Button color="success" style={{width:90}} 
                onClick={
                  ()=>{
                    swal.fire({
                      title: 'Pilih Status Verifikasi',
                      input: 'select',
                      inputOptions: {
                        true : 'Sudah Verifikasi',
                        false : 'Belum Verifikasi',
                      },
                      inputPlaceholder: 'Pilih Status',
                      showCancelButton: true,
                      inputValidator: (value) => {
                        return new Promise((resolve) => {
                          if (value === 'true') {
                            var db = firebase.firestore();
                            var update = db.collection("user").doc(data.id)
                            update.update({
                              verifikasi : true,
                            })
                            .then(()=>{
                              return swal.fire("Sukses","","success")
                            })
                          }else{
                            var db = firebase.firestore();
                            var update = db.collection("user").doc(data.id)
                            update.update({
                              verifikasi : false,
                            })
                            .then(()=>{
                              return swal.fire("Sukses","","success")
                            })
                          }
                        })
                      }

                    }) 
                }}>
                Sudah
              </Button>
              <Button style={{marginLeft:10}} outline color="info" onClick={()=>{localStorage.setItem('id_user',data.id);this.props.history.push('/info_user')}}>info</Button>
            </div>
            )}
            {renderIf(data.data.verifikasi === false)(
            <div>
              <Button color="warning" style={{width:90}} 
                onClick={
                  ()=>{
                    swal.fire({
                      title: 'Pilih Status Verifikasi',
                      input: 'select',
                      inputOptions: {
                        true : 'Sudah Verifikasi',
                        false : 'Belum Verifikasi',
                      },
                      inputPlaceholder: 'Pilih Status',
                      showCancelButton: true,
                      inputValidator: (value) => {
                        return new Promise((resolve) => {
                          if (value === 'true') {
                            var db = firebase.firestore();
                            var update = db.collection("user").doc(data.id)
                            update.update({
                              verifikasi : true,
                            })
                            .then(()=>{
                              return swal.fire("Sukses","","success")
                            })
                          }else{
                            var db = firebase.firestore();
                            var update = db.collection("user").doc(data.id)
                            update.update({
                              verifikasi : false,
                            })
                            .then(()=>{
                              return swal.fire("Sukses","","success")
                            })
                          }
                        })
                      }

                    }) 
                }}>
                Belum
              </Button>
              <Button style={{marginLeft:10}} outline color="info" onClick={()=>{localStorage.setItem('id_user',data.id);this.props.history.push('/info_user')}}>info</Button>
            </div>
            )}
          </div>
        ]
      )

    const options = {
      filterType: 'dropdown',
      print : false,
      download : false,
      selectableRows : 'none',
      viewColumns : false
    };

    const columns=[
      {
        name: "No Registrasi",
        label: "No Registrasi",
        options: {
         filter: false,
         sort: true,
        }
      },{
        name: "NIK",
        label: "NIK",
        options: {
         filter: false,
         sort: false,
        }
      },{
        name: "Nama Lengkap",
        label: "Nama Lengkap",
        options: {
         filter: true,
         sort: false,
        }
      },{
        name: "Pendaftaran",
        label: "Pendaftaran",
        options: {
         filter: true,
         sort: false,
        }
      },{
        name: "Status Verifikasi",
        label: "Status Verifikasi",
        options: {
         filter: false,
         sort: false,
        }
      }
    ]

    return (
      <div>
        <MUIDataTable
          title={"Pengguna"}
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    )
  }

}

export default bonus;
