import React, { Component } from 'react';
import {
  Button,
} from 'reactstrap';
import firebase from 'firebase'
import MUIDataTable from "mui-datatables";
import swal from 'sweetalert2';

class bonus extends Component {

    constructor(props){
        super(props);
        this.state = {
          member :[],
          id     :'',
          nomor  :'',
          modalIsOpen: false,
          performance : '',
          synergi:'',
          integrity:'',
          bussiness:'',
        }
      }

     componentWillMount(){
       const db = firebase.firestore();
       const docRef = db.collection("transaksi").orderBy('waktu', 'asc')
       docRef.onSnapshot(async(querySnapshot)=>{
        var data = []
        querySnapshot.forEach((doc)=>{
          let datas = data
          datas.push({
            id   : doc.id,
            data : doc.data()
          })
        })
        this.setState({
          member    : data,
          nomor     : Object.keys(data).length
        })
       
       })
    }

  render() {
    const {member} = this.state;
    const data = Object.values(member).map((data,index)=>
        [ 
          data.data.Donatur,
          data.data.Penerima,
          data.data.nama_transaksi,
          <div>
            <div>
              <Button color="success" style={{width:90}} 
                onClick={
                  ()=>{
                    swal.fire({
                      title: 'Pilih Status',
                      input: 'select',
                      inputOptions: {
                        'Menunggu': 'Menunggu',
                        'Proses': 'Proses',
                        'Selesai': 'Selesai',
                        'Gagal': 'Gagal'
                      },
                      inputPlaceholder: 'Pilih Status',
                      showCancelButton: true,
                      inputValidator: (value) => {
                        return new Promise((resolve) => {
                          if (value === value) {
                            var db = firebase.firestore();
                            var update = db.collection("transaksi").doc(data.id)
                            update.update({
                              status : value,
                            })
                            .then(()=>{
                              return swal.fire("Sukses","","success")
                            })
                          }
                        })
                      }

                    }) 
                }}>

                {data.data.status}
              </Button>
              <Button style={{marginLeft:10}} outline color="info">info</Button>
            </div>
          </div>
        ]
      )

    const options = {
      filterType: 'dropdown',
      print : false,
      download : false,
      selectableRows : 'none',
      viewColumns : false
    };

    const columns=[
      {
        name: "Pengenal",
        label: "Donatur",
        options: {
         filter: false,
         sort: false,
        }
      },{
        name: "Penerima",
        label: "Penerima",
        options: {
         filter: false,
         sort: false,
        }
      },{
        name: "Transaksi",
        label: "Transaksi",
        options: {
         filter: true,
         sort: false,
        }
      },{
        name: "Status",
        label: "Status",
        options: {
         filter: false,
         sort: false,
        }
      }
    ]

    return (
      <div>
        <MUIDataTable
          title={"Transaksi"}
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    )
  }

}

export default bonus;
