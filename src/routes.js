import React from 'react';

const Dashboard = React.lazy(() => import('./Dashboard'));

const Header = React.lazy(()=> import('./menu/web_limaf/header'))
const Contact = React.lazy(()=> import('./menu/web_limaf/contact'))

const About = React.lazy(()=> import('./menu/web_limaf/about/about'))
const List_about = React.lazy(()=> import('./menu/web_limaf/about/list'))
const Add_about = React.lazy(()=> import('./menu/web_limaf/about/add'))

const Team = React.lazy(()=> import('./menu/web_limaf/team/team'))
const List_team = React.lazy(()=> import('./menu/web_limaf/team/list'))
const Add_team = React.lazy(()=> import('./menu/web_limaf/team/add'))

const Port = React.lazy(()=> import('./menu/web_limaf/port/port'))
const List_port = React.lazy(()=> import('./menu/web_limaf/port/list'))
const Add_port = React.lazy(()=> import('./menu/web_limaf/port/add'))
const keluar = React.lazy(()=> import('./menu/keluar'))


const Pengguna = React.lazy(()=> import('./menu/ummiyatim/pengguna'))
const Transaksi = React.lazy(()=> import('./menu/ummiyatim/transaksi'))
const Info = React.lazy(()=> import('./menu/ummiyatim/info_user'))


const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/Biodata', name: 'Biodata Saya', component: Dashboard },
  ///web
  { path: '/Header', name: 'Header', component: Header },
  { path: '/Contact', name: 'Contact', component: Contact },
  ///
  { path: '/Port', name: 'Gallery', component: Port },
  { path: '/List_port', name: 'List', component: List_port },
  { path: '/Add_port', name: 'Tambah Data', component: Add_port },
  ///
  { path: '/Team', name: 'Team', component: Team },
  { path: '/List_team', name: 'List', component: List_team },
  { path: '/Add_team', name: 'Tambah Data', component: Add_team },
    ///
  { path: '/About', name: 'About', component: About },
  { path: '/List_about', name: 'List', component: List_about },
  { path: '/Add_about', name: 'Tambah Data', component: Add_about },
  ///keluar
  { path: '/Keluar', name: 'Keluar', component: keluar },
  ///Ummiyatim
  { path: '/Pengguna', name: 'Pengguna', component: Pengguna },
  { path: '/Transaksi', name: 'Transaksi', component: Transaksi },
  { path: '/info_user', name: 'Detail User', component: Info },

];

export default routes;
