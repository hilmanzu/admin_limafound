export default {
  items: [
    {
      name: 'Dashboard',
      url: '/Dashboard',
      icon: 'cui-home',
    },
    {
      name: 'Transaksi',
      url: '/Transaksi',
      icon: 'cui-comment-square-smile',
    },
    {
      name: 'Pengguna',
      url: '/Pengguna',
      icon: 'cui-comment-square-smile',
    },
    {
      name: 'Web_LF',
      icon: 'cui-settings',
      children: [
        {
          name: 'Header',
          url: '/Header',
        },{
          name: 'About',
          url: '/About',
        },{
          name: 'Team',
          url: '/Team',
        },{
          name: 'Gallery',
          url: '/Port',
        },{
          name: 'Contact',
          url: '/Contact',
        }
      ]
    },{
      name: 'Master',
      icon: 'cui-settings',
      children: [
        {
          name: 'Agama',
        },{
          name: 'Banner',
        },{
          name: 'Progress',
        },{
          name: 'Status Ekonomi',
        },{
          name: 'Status Ummi',
        }
      ]
    },{
      name: 'Keluar',
      url: '/Keluar',
      icon: 'icon-logout',
    }
  ],
};
